<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TracksUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('track_user', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('track_id');

//            $table->foreign('user_id')
//                  ->references('id')
//                  ->on('users');
//
//            $table->foreign('track_id')
//                  ->references('id')
//                  ->on('tracks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('track_user');
    }

}
