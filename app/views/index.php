<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>Spintech.dj</title>

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/styles.min.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var PageData = <?php echo json_encode($pageData); ?>;
        console.log(PageData);
    </script>
</head>

<body ng-app="spintech">
<div id="navbar-main" class="navbar navbar-inverse navbar-fixed-top" role="navigation" ng-controller="NavCtrl">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" ng-init="navCollapsed = true"
                    ng-click="navCollapsed = !navCollapsed">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <span>spintech.dj</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" ng-class="{'in':!navCollapsed}">
            <ul class="nav navbar-nav navbar-right">
                <!--                <li class="nav-home" ng-click="navCollapsed=true" ng-hide="userService.isLoggedIn()">-->
                <!--                    <a href="/">Home</a>-->
                <!--                </li>-->
                <li class="nav-dashboard" ng-click="navCollapsed=true" ng-show="userService.isLoggedIn()">
                    <a href="/#/dashboard">Dashboard</a>
                </li>
                <!--                <li class="nav-leaderboard" ng-click="navCollapsed=true">-->
                <!--                    <a href="/#/leaderboard">Leader Board</a>-->
                <!--                </li>-->

                <li class="nav-twitter" ng-hide="userService.isLoggedIn()">
                    <button type="button" ng-click="twitterLogin()" class="btn btn-default navbar-btn">
                        Login with Twitter
                        <i class="fa fa-twitter"></i>
                    </button>
                </li>

                <li ng-show="userService.isLoggedIn()" class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <img class="navbar-profile-img"
                             ng-src="{{userService.currentUser().twitter_profile.profile_image_url}}"
                             height="20"
                             width="20"/>
                        {{userService.currentUser().username}}
                        <span class="caret"></span>
                        <ul class="dropdown-menu" role="menu">
<!--                            <li class="nav-terms">-->
<!--                                <a href="/#/terms">Terms of Service</a>-->
<!--                            </li>-->
                            <li class="divider"></li>
                            <li class="nav-logout" ng-show="userService.isLoggedIn()">
                                <a href="/session/logout">Logout</a>
                            </li>
                        </ul>
                    </a>
                </li>

            </ul>

        </div>
    </div>
</div>

<!--  Main View for SPA -->
<div ng-view></div>


<nav id="navbar-footer" class="navbar navbar-default navbar-fixed-bottom" role="navigation"
     ng-controller="StreamPlayerCtrl">
    <div class="container">
        <ul class="nav navbar-right">
            <button type="button" class="btn btn-default navbar-btn btn-play" ng-click="togglePlayStream()">
                <i class="fa" ng-class="playButtonIcon"></i>
            </button>
            <button type="button" class="btn btn-default navbar-btn btn-mute" ng-disabled="muteButtonDisabled" ng-click="toggleMute()">
                <i class="fa" ng-class="muteButtonIcon"></i>
            </button>
            <button type="button" class="btn btn-default btn-disabled navbar-btn volume-display" disabled>
                <small>Volume {{volumeDisplay * 100 | number: 0}}% &nbsp;&nbsp;</small>
            </button>
        </ul>
    </div>
</nav>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-48564549-3', 'spintech.dj');
ga('send', 'pageview');
</script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="/js/vendor/fastclick/lib/fastclick.js"></script>
<script type="text/javascript" src="/js/vendor/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/js/vendor/angular/angular.min.js"></script>
<script type="text/javascript" src="/js/vendor/angular-route/angular-route.min.js"></script>
<script type="text/javascript" src="/js/vendor/angular-ui-bootstrap-bower/ui-bootstrap.min.js"></script>
<script type="text/javascript" src="/js/vendor/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="/js/vendor/angular-truncate/src/truncate.js"></script>
<script type="text/javascript" src="/js/services.js"></script>
<script type="text/javascript" src="/js/controllers.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
