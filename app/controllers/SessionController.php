<?php

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 6/16/14
 * Time: 12:16 AM
 */
class SessionController extends BaseController {

    public function login()
    {

    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

    public function twitterRequestToken()
    {
        Session::flush();
        $cb    = \Codebird\Codebird::getInstance();
        $reply = $cb->oauth_requestToken(array(
            'oauth_callback' => 'http://' . $_SERVER['HTTP_HOST'] . "/session/twitter/access_token"
        ));
        Session::forget('oauth_verify');

        // store the token
        $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);

        Session::set('oauth_token', $reply->oauth_token);
        Session::set('oauth_token_secret', $reply->oauth_token_secret);
        Session::set('oauth_verify', true);

        // redirect to auth website
        $auth_url = $cb->oauth_authorize();
        return Redirect::away($auth_url);
    }

    public function twitterAccessToken()
    {
        $cb = \Codebird\Codebird::getInstance();

        if (Input::has('oauth_verifier') && Session::has('oauth_verify'))
        {
            // verify the token
            $cb->setToken(Session::get('oauth_token'), Session::get('oauth_token_secret'));
            Session::forget('oauth_verify');

            // get the access token
            $reply = $cb->oauth_accessToken(array(
                'oauth_verifier' => Input::get('oauth_verifier')
            ));

            // store the token (which is different from the request token!)
            Session::set('oauth_token', $reply->oauth_token);
            Session::set('oauth_token_secret', $reply->oauth_token_secret);

            // check to see if there is already a user with this twitter id/screen_name
            $user = User::where('twitter_name', $reply->screen_name)->first();
            // if so, log him in
            if(!$user) {
                // otherwise create an account
                $user = User::create(array('username' => $reply->screen_name, 'twitter_name' => $reply->screen_name));
                $user->save();
            }


            Auth::loginUsingId($user->id);
            return Redirect::to('/#/dashboard');
        }
    }


}
