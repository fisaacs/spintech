<?php

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 7/9/14
 * Time: 11:36 PM
 */
class LeadersController extends \BaseController {

    /**
     * Get leader board stats
     * @return mixed
     */
    public function allTimeUsers()
    {
        return Response::json(User::getLeaders());
    }

    public function thisMonthUsers()
    {
        return Response::json(User::getMonthlyLeaders(date('m'), date('Y')));
    }

    public function allTimeTracks()
    {
        return Response::json(Track::getLeaders());
    }

    public function thisMonthTracks()
    {
        return Response::json(Track::getMonthlyLeaders(date('m'), date('Y')));
    }

    public function all()
    {
        $leaders = array(
            'users'  => array(
                'all_time'   => User::getLeaders(),
                'this_month' => User::getMonthlyLeaders(date('m'), date('Y'))
            ),
            'tracks' => array(
                'all_time'   => Track::getLeaders(),
                'this_month' => Track::getMonthlyLeaders(date('m'), date('Y'))
            ),
            'query_log' => DB::getQueryLog()
        );
        return Response::json($leaders);
    }
} 