<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function index()
    {
        $pageData = array(
            'user' => array(
                'id'              => 0,
                'twitter_profile' => array(
                    'profile_image_url' => ""
                )
            )
        );

        if (Auth::check())
        {
            $user     = User::find(Auth::user()->id);
            $pageData = array(
                'user' => $user->toArray()
            );
        }

        return View::make('index')->with('pageData', $pageData);
    }

}
