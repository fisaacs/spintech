<?php

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 6/16/14
 * Time: 12:54 AM
 */
class StreamController extends BaseController {

    private function saveTrack($data)
    {
        // does track exist?
        $track = Track::where('artworkID', $data['artworkID'])->where('title', $data['title'])->first();
        // if not create it
        if (!$track)
        {
            $track = Track::create($data);
            $track->save();
        }
        return $track;
    }

    private function getAlbumArtUrl($artwork_id)
    {
        $cacheKey = sprintf("album_art_%s", $artwork_id);
        
        $pngUrl = sprintf("http://spintech.dj/container/now_playing-%s.png", $artwork_id);
        $jpgUrl = sprintf("http://spintech.dj/container/now_playing-%s.jpg", $artwork_id);

        $response = HttpClient::head($pngUrl);
        if ($response->statusCode() == 200)
        {
            return $pngUrl;
        }

        $response = HttpClient::head($jpgUrl);
        if ($response->statusCode() == 200)
        {
            return $jpgUrl;
        }

        return "/img/logo.png";
    }


    public function current()
    {
        try
        {
            $xml = simplexml_load_file('http://spintech.dj/container/nowplaying.xml');
        }
        catch(Exception $ex)
        {
            return Response::json(array('error' => $ex->getMessage()), 500);
        }

        $result = array(
            'current'  => null,
            'previous' => array()
        );

        foreach ($xml->song as $song)
        {
            $order = intval($song->attributes()->order);
            $image = (string)$song->image;

            if (empty($image))
            {
                $image = $this->getAlbumArtUrl($song->artworkID);
            }

            $entry = array(
                "title"        => (string)$song->title,
                "artist"       => (string)$song->artist,
                "album"        => (string)$song->album,
                "genre"        => (string)$song->genre,
                "year"         => (string)$song->year,
                "image"        => (string)$image,
                "imageSmall"   => (string)$song->imageSmall,
                "imageLarge"   => (string)$song->imageLarge,
                'order'        => $order,
                'timestamp'    => (string)$song->attributes()->timestamp,
                'artworkID'    => (string)$song->artworkID,
                "link"         => (string)$song->comments,
                "twitter_name" => (string)$song->composer
            );

            $track = $this->saveTrack($entry);

            $entry['id'] = $track->id;

            if ($order == 1)
            {
                $result['current'] = $entry;
            }
            else
            {
                $result['previous'][] = $entry;
            }
        }

        return Response::json($result);

    }

} 