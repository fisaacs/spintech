<?php

class UsersController extends \BaseController {


    /**
     * GET /users/current
     *
     * @return string json user representation
     */
    public function current()
    {
        $user = array();
        if (Auth::check())
        {
            $user = User::with('favorites')->findOrFail(Auth::user()->id);
        }
        return Response::json($user);
    }

    /**
     * Display a listing of the resource.
     * GET /users
     *
     * @return string json
     */
    public function index()
    {
        $users = User::all();
        return Response::json($users);
    }

    /**
     * Display the specified resource.
     * GET /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = User::with('favorites')->findOrFail($id);
        return Response::json($user);
    }

    /**
     * Update the specified resource in storage.
     * PUT /users/{id}
     *
     * @param  int $id
     *
     * @throws Exception - method not implemented
     */
    public function update($id)
    {
        throw new Exception('Method not implemented');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     *
     * @throws Exception - method not implemented
     */
    public function destroy($id)
    {
        throw new Exception('Method not implemented');
    }


    /**
     * POST /users/favorites/{id}
     *
     * @param $track_id
     *
     * @return mixed
     */
    public function addFavorite($track_id)
    {

        $track = Track::findOrFail($track_id);
        $user  = User::findOrFail(Auth::user()->id);
        $cb    = \Codebird\Codebird::getInstance();

        $user->favorites()->attach($track->id);

        $reply = $cb->statuses_update('status=' . $track->getTweetFormat());

        return Response::json(array('status' => 'ok'));
    }



}