<?php

class TracksController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tracks
	 *
	 * @return Response
	 */
	public function index()
	{
		$tracks = Track::all();
        return Response::json($tracks);
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /tracks
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tracks/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$track = Track::with('usersWhoLike')->findOrFail(1);
        return Response::json($track);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tracks/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}



}