<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Main Route
 */
Route::get('/', 'HomeController@index');

/**
 * Session / Authentication Routes
 */
Route::get('/session/logout', 'SessionController@logout');
Route::get('/session/twitter/login', 'SessionController@twitterRequestToken');
Route::get('/session/twitter/access_token', 'SessionController@twitterAccessToken');

/**
 * API Routes
 */
Route::group(array('prefix' => 'api'), function ()
{
    // Stream information
    Route::get('/stream/current', 'StreamController@current');

    // RPC Endpoints for users
    Route::post('/users/favorites/{id}', 'UsersController@addFavorite');
    Route::get('/users/current', 'UsersController@current');

    // RPC Endpoints for leaderboards
    Route::get('/leaders', 'LeadersController@all');
    Route::get('/leaders/users/all-time', 'LeadersController@allTimeUsers');
    Route::get('/leaders/users/this-month', 'LeadersController@thisMonthUsers');
    Route::get('/leaders/tracks/all-time', 'LeadersController@allTimeTracks');
    Route::get('/leaders/tracks/this-month', 'LeadersController@thisMonthTracks');

    // REST Resource Routes
    Route::resource('tracks', 'TracksController');
    Route::resource('users', 'UsersController');
});


