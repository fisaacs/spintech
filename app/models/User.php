<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codebird\Codebird;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    protected $primary_key = "id";

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * @var array
     */
    protected $fillable = array('username', 'twitter_name');

    /**
     * Virtual Fields
     * @var array
     */
    protected $appends = array('twitter_profile');

    /**
     * Get this users favorites
     * @return mixed
     */
    public function favorites()
    {
        return $this->belongsToMany('Track')
                    ->withPivot('created_at')
                    ->orderBy('pivot_created_at', 'desc');
    }

    /**
     * accessor for custom field
     * @return mixed
     */
    public function getTwitterProfileAttribute()
    {
        $cacheKey     = sprintf("twitter-%d", $this->id);
        $twitter_name = $this->twitter_name;

        return Cache::remember($cacheKey, 60, function () use ($twitter_name)
        {
            $reply = CodeBird::getInstance()->users_show(array('screen_name' => $twitter_name));
            // hack to produce higher res profile images
            $reply->profile_image_url       = str_replace('normal', 'bigger', $reply->profile_image_url);
            $reply->profile_image_url_https = str_replace('normal', 'bigger', $reply->profile_image_url_https);
            return $reply;
        });
    }


    public static function getMonthlyLeaders($month, $year, $limit = 10)
    {
        $start = sprintf("%d-%02d-01 00:00:00", $year, $month);
        $end   = sprintf("%d-%02d-%02d 23:59:59", $year, $month, date('t', strtotime($start)));

        $counts = DB::table('track_user')
                    ->select(DB::raw('user_id, count(id) as hearts'))
                    ->where('created_at', '>=', $start)
                    ->where('created_at', '<=', $end)
                    ->groupBy('user_id')
                    ->orderBy('hearts', 'DESC')
                    ->limit($limit)
                    ->get();

        return self::buildLeaders($counts);
    }


    public static function getLeaders($limit = 10)
    {
        // get the user ids with the hundred highest count of track_user rows
        $counts = DB::table('track_user')
                    ->select(DB::raw('user_id, count(id) as hearts'))
                    ->groupBy('user_id')
                    ->orderBy('hearts', 'DESC')
                    ->limit($limit)
                    ->get();

        return self::buildLeaders($counts);
    }


    public static function buildLeaders($counts)
    {
        $leaders = array();
        foreach ($counts as $count)
        {
            $user   = User::find($count->user_id);
            $leader = array(
                'user'          => $user,
                'display_image' => $user->twitter_profile->profile_image_url,
                'hearts'        => $count->hearts,
                'last_heart'    => $user->favorites()->first()
            );

            $leaders[] = $leader;
        }
        return $leaders;
    }

}
