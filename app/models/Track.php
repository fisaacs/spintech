<?php

class Track extends \Eloquent {

    protected $table = "tracks";

    protected $fillable = [
        'title',
        'artist',
        'album',
        'genre',
        'year',
        'image',
        'artworkID',
        'link',
        'twitter_name'
    ];

    public function usersWhoLike()
    {
        return $this->belongsToMany('User');
    }

    private function truncate($text, $chars = 25)
    {
        $text = $text . " ";
        $text = substr($text, 0, $chars);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text . "...";
        return $text;

    }

    public function getTweetFormat()
    {

        $format = 'I ♥️ "%s" by %s on @spintechdj. %s';

        $artist = $this->artist;
        if (!empty($this->twitter_name))
        {
            $artist = $this->twitter_name;
        }

        return sprintf($format, $this->title, $artist, $this->link);
    }

    public static function buildLeaders($counts)
    {
        $leaders = array();
        foreach ($counts as $count)
        {
            $track  = Track::find($count->track_id);
            $leader = array(
                'track'  => $track->toArray(),
                'hearts' => $count->hearts
            );

            $leaders[] = $leader;
        }
        return $leaders;
    }


    public static function getLeaders($limit = 10)
    {
        // get the user ids with the hundred highest count of track_user rows
        $counts = DB::table('track_user')
                    ->select(DB::raw('track_id, count(id) as hearts'))
                    ->groupBy('track_id')
                    ->orderBy('hearts', 'DESC')
                    ->limit($limit)
                    ->get();

        return self::buildLeaders($counts);
    }

    public static function getMonthlyLeaders($month, $year, $limit = 10)
    {
        $start = sprintf("%d-%02d-01 00:00:00", $year, $month);
        $end   = sprintf("%d-%02d-%02d 23:59:59", $year, $month, date('t', strtotime($start)));

        $counts = DB::table('track_user')
                    ->select(DB::raw('track_id, count(id) as hearts'))
                    ->where('created_at', '>=', $start)
                    ->where('created_at', '<=', $end)
                    ->groupBy('track_id')
                    ->orderBy('hearts', 'DESC')
                    ->limit($limit)
                    ->get();

        return self::buildLeaders($counts);
    }

}