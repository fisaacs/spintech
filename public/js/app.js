/**
 * Created by fred on 6/14/14.
 */
angular.module('spintech', ['ngRoute', 'spintech.controllers'])
    .config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: '/js/templates/home.html',
                    controller: 'HomeCtrl'
                })
                .when('/dashboard', {
                    templateUrl: '/js/templates/dashboard.html',
                    controller: 'DashboardCtrl'
                })
                .when('/leaderboard', {
                    templateUrl: '/js/templates/leaderboard.html',
                    controller: 'LeaderBoardCtrl'
                })
                .when('/terms', {
                    templateUrl: '/js/templates/terms.html'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }])
    .filter('timeago', function () {
        return function (input) {
            return moment.utc(input).from();
        };
    })
    .run(function () {
        FastClick.attach(document.body);
    });