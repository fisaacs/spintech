/**
 * Created by fred on 6/14/14.
 */
angular.module('spintech.controllers', ['ui.bootstrap', 'spintech.services', 'truncate'])
    .controller('StreamPlayerCtrl', function($scope) {
        $scope.audio = null;
        $scope.muteButtonIcon = "fa-volume-up";
        $scope.muteButtonDisabled = true;

        $scope.playButtonIcon = "fa-play";
        $scope.volumeDisplay = 1;

        $scope.togglePlayStream = function() {
            if($scope.audio == null) {
                $scope.audio = new Audio('http://spintech.dj:8001/');
                $scope.audio.preload = true;
                $scope.playButtonIcon = "fa-refresh fa-spin";
                $scope.muteButtonDisabled = false;
                $scope.audio.play();
                $scope.audio.addEventListener('playing', function() {
                    $scope.playButtonIcon = 'fa-stop';
                });
                $scope.audio.addEventListener('waiting', function() {
                    $scope.playButtonIcon = 'fa-refresh fa-spin';
                });
                $scope.audio.addEventListener('stalled', function() {
                    $scope.playButtonIcon = "fa-exclamation-circle";
                });

            } else {
                $scope.audio.pause();
                $scope.audio = null;
                $scope.playButtonIcon = 'fa-play';
                $scope.muteButtonDisabled = true;
            }
        }

        $scope.toggleMute = function() {
            if(null == $scope.audio) {
                return;
            }
            if($scope.audio.muted) {
                $scope.audio.muted = false;
                $scope.muteButtonDisabled = false;
                $scope.muteButtonIcon = "fa-volume-up";
                $scope.volumeDisplay = 1;
            } else {
                $scope.audio.muted = true;
                $scope.muteButtonDisabled = false;
                $scope.muteButtonIcon = "fa-volume-off";
                $scope.volumeDisplay = 0;
            }
        }

    })
    .controller('NavCtrl', function($scope, $modal, $log, UserService) {
        $scope.selected = null;

        $scope.userService = UserService;

        $scope.twitterLogin = function() {
            window.location = "/session/twitter/login";
        }
    })
    .controller('LoginCtrl', function($scope, $modalInstance) {
        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    })
    .controller('LeaderBoardCtrl', function($scope, $http, $log) {
        $scope.leaders = [];
        $http.get('/api/leaders').success(function(response) {
            $scope.leaders = response;
        });
    })
    .controller('DashboardCtrl', function($scope, $http, $log, UserService) {
        $scope.streamMetaAvailable = true;
        $scope.userService = UserService;
        $scope.favorites = [];

        $scope.stream = {
            current: {},
            previous: []
        };

        $scope.playlist = function() {
            $http.get('/api/stream/current').success(function(response) {
                $log.info(response);
                $scope.stream = response;
                $scope.streamMetaAvailable = true;
            }).error(function(response) {
                $scope.streamMetaAvailable = false;
            });
        }

        $scope.loadFavorites = function() {
            if(UserService.isLoggedIn()) {
                $http.get('/api/users/current').success(function(response) {
                    $scope.favorites = response.favorites;
                })
            }
        }

        $scope.addToFavorites = function(track) {
            $log.info(track, 'Add To Favorites');

            $http.post('/api/users/favorites/' + track.id).success(function(response) {
                console.log('favorites added');
                $scope.loadFavorites();
            });
        }

        $scope.trackIsFavorite = function(track) {
            var isFavorite = false;
            angular.forEach($scope.favorites, function(value, key) {
                if(track.id == value.id) {
                    isFavorite = true
                }
            });

            return isFavorite;
        }

        $scope.playlist();

        setInterval($scope.playlist, 15000);

        if(UserService.isLoggedIn()) {
            $scope.loadFavorites();
        }
    })
    .controller('HomeCtrl', function($scope, $http, $log, UserService) {

        $scope.streamMetaAvailable = true;
        $scope.userService = UserService;
        $scope.leaders = [];
        $scope.favorites = [];

        $scope.stream = {
            current: {},
            previous: []
        };


        $scope.playlist = function() {
            $http.get('/api/stream/current').success(function(response) {
                $log.info(response);
                $scope.stream = response;
                $scope.streamMetaAvailable = true;
            }).error(function(response) {
                $scope.streamMetaAvailable = false;
            });
        }


        $scope.loadFavorites = function() {
            if(UserService.isLoggedIn()) {
                $http.get('/api/users/current').success(function(response) {
                    $scope.favorites = response.favorites;
                })
            }
        }

        $scope.addToFavorites = function(track) {
            $log.info(track, 'Add To Favorites');

            $http.post('/api/users/favorites/' + track.id).success(function(response) {
                console.log('favorites added');
                $scope.loadFavorites();
            });
        }

        $scope.trackIsFavorite = function(track) {
            var isFavorite = false;
            angular.forEach($scope.favorites, function(value, key) {
                if(track.id == value.id) {
                    isFavorite = true
                }
            });

            return isFavorite;
        }

        $scope.playlist();

        setInterval($scope.playlist, 5000);

        $http.get('/api/leaders').success(function(response) {
            $scope.leaders = response;
        });

        if(UserService.isLoggedIn()) {
            $scope.loadFavorites();
        }
    });