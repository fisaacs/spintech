/**
 * Created by fred on 6/17/14.
 */
angular.module('spintech.services', [])
    .factory('UserService', function ($http) {
        var currentUser = PageData.user;

        return {
            isLoggedIn: function () {
                return (angular.isDefined(currentUser.id) && currentUser.id != 0);
            },
            currentUser: function () {
                return currentUser;
            }
        };
    });