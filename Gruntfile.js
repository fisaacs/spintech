/**
 * Created by fred on 11/1/13.
 */
'use strict';
module.exports = function (grunt) {

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'public/js/*.js'
            ]
        },
        less: {
            dist: {
                options: {
                    compile: true,
                    compress: true
                },
                files: {
                    'public/css/styles.min.css': [
                        'public/less/styles.less'
                    ]
                }
            }
        },
//    uglify: {
//      dist: {
//        files: {
//          'js/scripts.min.js': [
//            //array of file names
//          ]
//        }
//      }
//    },
        watch: {
            less: {
                files: [
                    'public/less/*.less',
                    'public/less/bootstrap/*.less',
                    'public/less/cyborg/*.less'
                ],
                tasks: ['less']
            },
            js: {
                files: [
                    '<%= jshint.all %>'
                ],
                tasks: ['jshint']
            }
        },
        clean: {
            dist: [
                'public/css/styles.min.css',
                'public/js/scripts.min.js'
            ]
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
//  grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Register tasks
    grunt.registerTask('default', [
        'clean',
        'less'
//    'uglify',
//    'version'
    ]);
    grunt.registerTask('dev', [
        'watch'
    ]);

};
